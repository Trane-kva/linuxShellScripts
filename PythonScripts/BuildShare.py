import os
import subprocess
import pwd
import grp


users = []
userObjects = []
path = './Share/'


def main():
    global users, userObjects
    with open('Users', 'r') as userList:
        for line in userList:
            users.append(line.strip())
    for user in users:
        userObjects.append(User(user, users))
    for user in userObjects:
        print(user)
    buildStructure()


def createUsers():
    global userObjects
    for User in userObjects:
        subprocess.call("sudo", 'ma')


def buildStructure():
    global userObjects, path
    os.mkdir(path, 0o775)
    os.mkdir(path + "/02_Blanks")
    admin_uid = pwd.getpwnam('administrator').pw_uid
    admin_gid = grp.getgrnam('administrator').gr_gid
    os.chown(path, admin_uid, admin_gid)
    os.chown(path + "/02_Blanks", admin_uid, admin_gid)
    for user in userObjects:
        os.mkdir(path + user.name + "/", 0o775)
        os.mkdir(path + user.name + "/01_Personal", 0x770)
        uid = pwd.getpwnam(user.name).pw_uid
        gid = grp.getgrnam(user.name).gr_gid
        os.chown(path + user.name + "/", uid, gid)
        os.chown(path + user.name + "/01_Personal", uid, gid)
        for guest in user.guests:
            os.mkdir(path + user.name + "/" + guest, 0o770)
            os.chown(path + user.name + "/" + guest, uid, gid)
            subprocess.call(['setfacl', '-R', '-m', 'user:' + guest + ':rwx',
                            path + user.name + "/" + guest])


class User:
    name = None
    guests = None

    def __init__(self, name, users):
        self.name = name
        self.guests = []
        self.defineGuests(users)

    def __str__(self):
        return self.name + "\n" + str(self.guests)

    def defineGuests(self, users):
        for user in users:
            if self.name not in user:
                self.guests.append(user)


if __name__ == '__main__':
    main()
