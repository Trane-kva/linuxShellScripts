import os
import sys
import subprocess
import pwd
import grp


users = []
path = './Share/'


def main():
    userName = input("Please enter user's name: ")
    loadUserFile()
    checkUser(userName)
    print(type(userName))
    addUserToSystem(userName)
    addUserToShareSystem(userName)


def loadUserFile():
    global users
    with open('Users', 'r') as userFile:
        for line in userFile:
            users.append(userFile.readline)
        print(len(users))


def checkUser(userName):
    if userName in users:
        print("Such user already exists")
        sys.exit(1)


def addUserToSystem(userName):
    password = userName + '123\n'
    password = password + password
    subprocess.call(['useradd', userName, '-p', userName + '123'])
    print("User was added to system")
    proc = subprocess.Popen(['smbpasswd', '-a', userName],
                            stdin=subprocess.PIPE)
    proc.communicate(password.encode())
    print("User was added to samba")


def addUserToShareSystem(userName):
    global users
    os.mkdir(path + userName , 0o775)
    os.mkdir(path + userName + "/01_Personal", 0x770)
    uid = pwd.getpwnam(userName).pw_uid
    gid = grp.getgrnam(userName).gr_gid
    os.chown(path + userName, uid, gid)
    os.chown(path + userName + "/01_Personal", uid, gid)
    for user in users:
        os.mkdir(path + user + "/" + userName, 0o770)
        os.chown(path + user + "/" + userName, uid, gid)
        subprocess.call(['setfacl', '-R', '-m', 'user:' + userName + ':rwx',
                        path + userName + "/" + userName])


if __name__ == '__main__':
    main()
