#!bin/bash

files="/backups/*.tar"
sDate=$(date +%d.%m.%y" "%T)
logDirectory="/home/trane/logs/cleanerLog.txt"

echo "=======================================================" >> $logDirectory
echo "$sDate cleaner start" >> $logDirectory
echo "Files deleted: " >> $logDirectory

#mount -o rw UUID=4838a567-77f6-4150-bf35-344ba1b4f7c0  /backups &&

#sleep 10 &&

find $files -type f -print -delete >> $logDirectory &&

UsedSpace=$(du -sh /backups/) &&

echo "Size: $UsedSpace" >> $logDirectory

EstDiskSpace=$(df -hT /backups) &&

echo "$EstDiskSpace" >> $logDirectory

eDate=$(date +%d_%m_%y" "%T)

/Scripts/Sendmail.sh "CleanerLog $eDate" "Monthly clean completed. \nSize: $UsedSpace \n$EstDiskSpace" >> $logDirectory &&

#sleep 60 &&

#umount /backups &&

echo "$eDate clean finished" >> $logDirectory
echo '=======================================================\n' >> $logDirectory
