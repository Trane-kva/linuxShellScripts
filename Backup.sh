#!/bin/bash

DATE=$(date +"%d.%m.%y") &&

sDate=$(date +%d.%m.%y" "%T) &&

logDirectory="/logs/BackupLog.txt" &&

#mount -o rw UUID=4838a567-77f6-4150-bf35-344ba1b4f7c0  /backups &&

#sleep 10 &&

#sudo tar -cpvf /backups/"$DATE($TIME)".tar /home/trane/Музыка &&

echo "=================================================================" >> $logDirectory
echo "Script starts: $sDate" >> $logDirectory

tar -cpvf /backups/"$DATE".tar /home/trane/Музыка >> $logDirectory &&

UsedSpace=$(du -sh /backups/$DATE.tar) &&

echo "Size: $UsedSpace" >> $logDirectory

EstDiskSpace=$(df -hT /backups) &&

echo "$EstDiskSpace" >> $logDirectory

eDate=$(date +%d.%m.%y" "%T) &&

/Scripts/Sendmail.sh "BackupLog $eDate" "Backup created. \nSize: $UsedSpace \n$EstDiskSpace" >> $logDirectory &&

echo "Script completed: $eDate" >> $logDirectory

#sleep 60 &&

#sudo umount /backups &&

echo "=================================================================" >> $logDirectory
echo '\n' >> $logDirectory
